/*Fonctions de rotation*/

function rotate(angle) {

	document.getElementById('elementRotate').style.transform="rotate("+angle+"deg)";
}

function rotateX(angle) {
	document.getElementById('rotateX').style.transform="rotateX("+angle+"deg)";
}

function rotateY(angle) {
	document.getElementById('rotateY').style.transform="rotateY("+angle+"deg)";
}

function rotateZ(angle) {
	document.getElementById('rotateZ').style.transform="rotateZ("+angle+"deg)";
}

function rotate3d(x, y, z, angle) {
	document.getElementById('elementRotate3D').style.transform="rotate3d("+x+", "+y+", "+z+", "+angle+"deg)";
}

/*Fonctions de translation*/

function translate (x, y) {
	if(y == "") {
		document.getElementById('elementTranslate').style.transform="translate("+x+"px)";
	}
	else {
		document.getElementById('elementTranslate').style.transform="translate("+x+"px,"+y+"px)";
	}
	
}

function translateX(x) {
	document.getElementById('elementTranslateX').style.transform="translateX("+x+"px)";
}

function translateY(y) {
	document.getElementById('elementTranslateY').style.transform="translateY("+y+"px)";
}

/*Fonctions d'échelle*/

function scale(x, y) {
	if(y == "") {
		document.getElementById('elementScale').style.transform="scale("+x+")";
	}
	else {
		document.getElementById('elementScale').style.transform="scale("+x+","+y+")";
	}	
}

function scaleX(x) {
	document.getElementById('elementScaleX').style.transform="scaleX("+x+")";
}

function scaleY(y) {
	document.getElementById('elementScaleY').style.transform="scaleY("+y+")";
}

/*Fonctions d'inclinaison*/

function skew(x, y) {
	document.getElementById('elementSkew').style.transform="skew("+x+"deg, "+y+"deg)";
}

function skewX(x) {
	document.getElementById('elementSkewX').style.transform="skewX("+x+"deg)";
}

function skewY(y) {
	document.getElementById('elementSkewY').style.transform="skewY("+y+"deg)";
}

/*Matrix*/
function matrix(a, b, c, d, e, f) {
	document.getElementById('elementMatrix').style.transform="matrix("+a+", "+b+", "+c+", "+d+", "+e+", "+f+")";
}

