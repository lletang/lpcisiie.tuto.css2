window.onload = function () {
}

function updateOpacity(val) {
	document.getElementById("sliderOpacity").firstElementChild.innerText = "-webkit-filter: opacity("+val+");";
	document.getElementById("image-1").style.opacity = val;
}

function updateBlur(val) {
	document.getElementById("sliderBlur").firstElementChild.innerText = "-webkit-filter: blur("+ val+"px);";
	document.getElementById("image-2").style.webkitFilter = "blur("+val+"px)";
}

function updateBrightness(val) {
	document.getElementById("sliderBrightness").firstElementChild.innerText = "-webkit-filter: brightness("+val+");";
	document.getElementById("image-3").style.webkitFilter = "brightness("+ val+"%)";
}

function updateContrast(val) {
	document.getElementById("sliderContrast").firstElementChild.innerText = "-webkit-filter: contrast("+val+");";
	document.getElementById("image-4").style.webkitFilter = "contrast(" + val +"%)";
}

function updateDropShadow(val) {
	var d = document.getElementById("image-5");
	//d.className = d.className + "dropshadow";
	if(d.className==""){
		d.className = "dropshadow";
	}else{
		d.className = "";	
	}
}

function updateGrayscale(val) {
	document.getElementById("sliderGrayscale").firstElementChild.innerText = "-webkit-filter: grayscale("+val+");";
	document.getElementById("image-6").style.webkitFilter = "grayscale(" + val +"%)";
}

function updateHueRotate(val) {
	document.getElementById("sliderHueRotate").firstElementChild.innerText = "-webkit-filter: hue-rotate("+val+");";
	document.getElementById("image-7").style.webkitFilter = "hue-rotate("+val+"deg)";
}

function updateInvert(val) {
	document.getElementById("sliderInvert").firstElementChild.innerText = "-webkit-filter: invert("+val+");";
	document.getElementById("image-8").style.webkitFilter = "invert(" + val +")";
}

function updateSaturate(val) {
	document.getElementById("sliderSaturate").firstElementChild.innerText = "-webkit-filter: saturate("+val+");";
	document.getElementById("image-9").style.webkitFilter = "saturate(" + val + ")";
}

function updateSepia(val) {
	document.getElementById("sliderSepia").firstElementChild.innerText = "-webkit-filter: sepia("+val+");";
	document.getElementById("image-10").style.webkitFilter = "sepia(" + val + ")";
}

