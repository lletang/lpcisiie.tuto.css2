# Tutoriel CSS

## Auteurs

* Guillaume Migeon
* Nicolas Obara
* Christophe Sauder

## Liste des fonction css
[source](https://developer.mozilla.org/fr/docs/Web/CSS/filter) 

* url(ressource) 
* blur(rayon) -----------FAIT
* brightness(quantité) --FAIT
* contrast(quantité) ----FAIT
* drop-shadow(<shadow>) -FAIT
* grayscale(quantité) ---FAIT
* hue-rotate(angle) -----FAIT
* invert(quantité) ------FAIT
* opacity(quantité) -----FAIT
* saturate(quantité) ----FAIT
* sepia(quantité) -------FAIT
