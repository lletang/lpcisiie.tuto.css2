#Auteurs
Brian Nallamoutou
Alexis Magron
Théo Gerriet


#Tutoriel : Blending-modes

Le Blend-mode est une fonctionnalité CSS qui permet de mélanger deux élément HTML. Il est possible de fusionner les images ou encore du texte de différentes façons.

Dans le répertoire se trouve trois fichiers d'exemples sur les blending-modes.

Le premier exemple expliquera la fusion d'une image avec une couleur de fond définie uniquement en HTML. Dans le deuxième, nous verrons la fusion de deux images différentes. Le troisième exemple vous montrera que les blending-modes ne se limitent pas aux images mais aussi au texte.

